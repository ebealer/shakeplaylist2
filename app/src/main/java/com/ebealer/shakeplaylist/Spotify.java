package com.ebealer.shakeplaylist;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by ebeal on 12/4/2015.
 */
public final class Spotify {
    public static String trackName = "";
    public static String artistName = "";
    public static Boolean playingState = false;
    public static String lastSavedTrack = "null";
    public static String lastSavedArtist = "null";
    public static String trackUri = "";

    private static User currentUser;

    public Spotify(User user) {
        currentUser = user;
    }

    public void setCurrentTrack(String track){
        trackName = track;
    }

    public void setCurrentArtist(String artist){
        artistName = artist;
    }

    public void setPlayingState(Boolean playing){
        playingState = playing;
    }

    public void setTrackUri(String uri){
        trackUri = uri;
    }

    public String getCurrentTrack(){
        return trackName;
    }

    public String getCurrentArtist(){
        return artistName;
    }

    public Boolean getPlayingState(){
        return playingState;
    }



    public static void saveCurrent(){
        if (trackName != lastSavedTrack){
            if (artistName != lastSavedArtist){
                Log.d("MY_DEBUG_TAG", "Saving track: " + artistName + " - " + trackName);
                try {
                    //if track uri == null then grab track name and find relevant uri
                    if (trackUri == null){
                        Spotify.addToPlaylist(getTrackUri());
                    } else {
                        Spotify.addToPlaylist(trackUri);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                lastSavedArtist = artistName;
                lastSavedTrack = trackName;
            }
        }
    }

    //Calls Spotify API and adds to playlist.
    public static void addToPlaylist(String uri) throws UnsupportedEncodingException {
      //  Log.d("URL",URLEncoder.encode(uri,"UTF-8") );
        String url = "https://api.spotify.com/v1/users/" + currentUser.getUserID() + "/playlists/" + currentUser.getUserCurrentPlaylistID() + "/tracks?position=0&uris=" + URLEncoder.encode(uri,"UTF-8");
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());

          //  Log.d("ADD TO RESPONSE CODE", Integer.toString(con.getResponseCode()));
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

         //   Log.d("OUTPUTLOG", response.toString());
        } catch (MalformedURLException e) {
            System.out.print("Failed at addToPlaylist()");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.print("Failed at addToPlaylist()");
            e.printStackTrace();
        }
    }

    public JSONArray getPlaylistList() {

        String url = "https://api.spotify.com/v1/me/playlists";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());

          //  Log.d("ADD TO RESPONSE CODE", Integer.toString(con.getResponseCode()));
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            try {
                JSONObject playlists = new JSONObject(response.toString());
                JSONArray items = playlists.getJSONArray("items");
                return items;
            } catch (JSONException e) {
                e.printStackTrace();
            }

          //  Log.d("OUTPUTLOG", response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String setUserId() {

        String url = "https://api.spotify.com/v1/me";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());

           // Log.d("ADD TO RESPONSE CODE", Integer.toString(con.getResponseCode()));
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            try {
                JSONObject user = new JSONObject(response.toString());
                currentUser.setUserID(user.getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

           // Log.d("OUTPUTLOG", response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String createPlaylist(String name) {

        String url = "https://api.spotify.com/v1/users/" + currentUser.getUserID() + "/playlists";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());
            con.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(
                    con.getOutputStream());
            out.write("{\"name\":\"" + name + "\",\"public\":true}");
            out.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            try {
                JSONObject res = new JSONObject(String.valueOf(response));
                currentUser.setUserCurrentPlaylistID(res.getString("id"));
                currentUser.setUserCurrentPlaylistName(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }

           // Log.d("RESPONSE", Integer.toString(con.getResponseCode()));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String setDisplayName() {

        String url = "https://api.spotify.com/v1/me/";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            try {
                JSONObject user = new JSONObject(response.toString());
                String items = user.getString("display_name");

               // Log.d("USERNAME", "This is what was gotten for a username: " + items);
                currentUser.setUserDisplayName(items);
            } catch (JSONException e) {
                e.printStackTrace();
            }

           // Log.d("OUTPUTLOG", response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTrackUri() throws UnsupportedEncodingException {
        String trackEncoded = URLEncoder.encode(trackName,"UTF-8");


        String url = "https://api.spotify.com/v1/search?q=" + trackEncoded+ "&type=track&limit=1";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "Bearer " + currentUser.getUserOath());

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            try {
                //tracks { items [ id ] }
                JSONObject tempTrackObj = new JSONObject(response.toString());
                JSONArray tempItemsArray = tempTrackObj.getJSONArray("tracks");
                JSONArray tempArtistArray = tempItemsArray.getJSONArray(1);
                String id = tempItemsArray.get(9).toString();
                String tempTrack = tempItemsArray.get(10).toString();
                String tempArtist = tempArtistArray.getString(3);
                trackName = tempTrack;
                artistName = tempArtist;
                trackUri = id;
                Log.d("DATACOL", "TRACKNAME: " + trackName + " ARTIST: " + artistName + " TRACKURI: " + trackUri);
                return trackUri;
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Log.d("OUTPUTLOG", response.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
