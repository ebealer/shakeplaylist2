package com.ebealer.shakeplaylist;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class User{
    private static String userID;
    private static String userDisplayName;
    private static Boolean userIsLoggedIn;
    private static String userCurrentPlaylistID;
    private static String userCurrentPlaylistName;
    private static Track userLastSavedTrack;
    private static String userOath;

    public static String getUserID() {
        return userID;
    }

    public static void setUserID(String userID) {
        User.userID = userID;
    }

    public static String getUserDisplayName() {
        return userDisplayName;
    }

    public static void setUserDisplayName(String userDisplayName) {
        User.userDisplayName = userDisplayName;
    }

    public static Boolean getUserIsLoggedIn() {
        return userIsLoggedIn;
    }

    public static void setUserIsLoggedIn(Boolean userIsLoggedIn) {
        User.userIsLoggedIn = userIsLoggedIn;
    }

    public static String getUserCurrentPlaylistID() {
        return userCurrentPlaylistID;
    }

    public static void setUserCurrentPlaylistID(String userCurrentPlaylistID) {
        User.userCurrentPlaylistID = userCurrentPlaylistID;
    }

    public static String getUserCurrentPlaylistName() {
        return userCurrentPlaylistName;
    }

    public static void setUserCurrentPlaylistName(String userCurrentPlaylistName) {
        User.userCurrentPlaylistName = userCurrentPlaylistName;
    }

    public static Track getUserLastSavedTrack() {
        return userLastSavedTrack;
    }

    public static void setUserLastSavedTrack(Track userLastSavedTrack) {
        User.userLastSavedTrack = userLastSavedTrack;
    }

    public static String getUserOath() {
        return userOath;
    }

    public static void setUserOath(String userOath) {
        User.userOath = userOath;
    }

    public User(){
    }

}
