package com.ebealer.shakeplaylist;

/**
 * Created by ebealer on 7/7/2017.
 */

public class Track {
    public static String trackID;
    public static String trackArtist;
    public static String trackName;
    public static String trackURI;

    public static String getTrackID() {
        return trackID;
    }

    public static void setTrackID(String trackID) {
        Track.trackID = trackID;
    }

    public static String getTrackArtist() {
        return trackArtist;
    }

    public static void setTrackArtist(String trackArtist) {
        Track.trackArtist = trackArtist;
    }

    public static String getTrackName() {
        return trackName;
    }

    public static void setTrackName(String trackName) {
        Track.trackName = trackName;
    }

    public static String getTrackURI() {
        return trackURI;
    }

    public static void setTrackURI(String trackURI) {
        Track.trackURI = trackURI;
    }
}
