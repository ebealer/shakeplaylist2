package com.ebealer.shakeplaylist;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by ebeal on 12/4/2015.
 */
public class MusicReceiver extends BroadcastReceiver{

    static final class BroadcastTypes {
        static final String SPOTIFY_PACKAGE = "com.spotify.music";
        static final String SPOTIFY_PLAYBACK_STATE_CHANGED = SPOTIFY_PACKAGE + ".playbackstatechanged";
        static final String SPOTIFY_QUEUE_CHANGED = SPOTIFY_PACKAGE + ".queuechanged";
        static final String SPOTIFY_METADATA_CHANGED = SPOTIFY_PACKAGE + ".metadatachanged";
        static final String GENERIC_METADATA_CHANGED = "com.android.music.metachanged";
    }

    @Override
    public void onReceive(Context context, Intent intent){
        long timeSentInMs = intent.getLongExtra("timeSent", 0L);

        String action = intent.getAction();

        if (action.equals(BroadcastTypes.SPOTIFY_METADATA_CHANGED)) {
            String trackId = intent.getStringExtra("id");
            String artistName = intent.getStringExtra("artist");
            String albumName = intent.getStringExtra("album");
            String trackName = intent.getStringExtra("track");
            int trackLengthInSec = intent.getIntExtra("length", 0);
            Spotify.artistName = artistName;
            Spotify.trackName = trackName;
            Spotify.playingState = true;
            //if trackid == null search for track id
            Spotify.trackUri = trackId;

            Log.v("SongChange", artistName + " - " + trackName);
        } else if (action.equals(BroadcastTypes.SPOTIFY_PLAYBACK_STATE_CHANGED)) {
            boolean playing = intent.getBooleanExtra("playing", false);
            int positionInMs = intent.getIntExtra("playbackPosition", 0);
            Spotify.playingState = playing;
        } else if (action.equals(BroadcastTypes.SPOTIFY_QUEUE_CHANGED)) {
            // Sent only as a notification, your app may want to respond accordingly.
        } else {
            Spotify.trackUri = null;
            //String trackId = intent.getStringExtra("id");
            String artistName = intent.getStringExtra("artist");
            //String albumName = intent.getStringExtra("album");
            String trackName = intent.getStringExtra("track");
            //int trackLengthInSec = intent.getIntExtra("length", 0);
            Spotify.artistName = artistName;
            Spotify.trackName = trackName;
            Spotify.playingState = true;
            //if trackid == null search for track id
            Log.d("Playing ", Spotify.trackName + " was trying to be saved from another source - OTHER - " + intent.getStringExtra("id") + " 00 " + intent.getStringExtra("artist") + " 00 " + intent.getStringExtra("track"));
            //THIS IS WHERE I WILL LOOK UP A SPOITIFY ID FOR THE TRACK. THIS ALLOWS FOR ANY MEDIA PLAYER TO ADD TO SPOTIFY

        }
    }
}
