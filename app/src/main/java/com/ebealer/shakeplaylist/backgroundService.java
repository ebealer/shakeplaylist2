package com.ebealer.shakeplaylist;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.annotation.IntDef;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class backgroundService extends Service {

    private Spotify s;
    private User currentUser;
    public backgroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(this,"Start Service", Toast.LENGTH_SHORT).show();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("ShakePlaylist")
                .setContentText("ShakePlaylist is waiting to be shaken")
                .setSmallIcon(R.drawable.ic_action_name)
                .setContentIntent(pendingIntent)
                .setTicker("ShakePlaylist started")
                .setSubText("Click to configure")

                .build();

        startForeground(80125, notification);

       //User currentUser = intent.getExtras().getParcelable("User");
        //Spotify s = new Spotify(currentUser);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        final Handler mHandler = new Handler();
        currentUser = new User();
        s = new Spotify(currentUser);
        vibra = new Thread(new Runnable() {
            @Override
            public void run() {


                while (true) {
                    try {
                        Thread.sleep(1000); //Time to check 1s
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mAccel > 8) { //Threshhold for phone shaking
                                    vibrator.vibrate(300);
                                    MainActivity.savePreferences();
                                    s.saveCurrent();
                                }
                            }
                        });
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        });
        vibra.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        vibra.interrupt();
        super.onDestroy();
    }

    //Checks for phone shaking
    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    public Vibrator vibrator;
    private Thread vibra;

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
}