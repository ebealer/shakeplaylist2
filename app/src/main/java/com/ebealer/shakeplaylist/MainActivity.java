package com.ebealer.shakeplaylist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String REDIRECT_URI = "shakeplaylist://callback";

    private static final int REQUEST_CODE = 8012;
    private static final String CLIENT_ID = "c59d01c3908e48c689c8725a8987adf2";
    private static SharedPreferences pref = null;
    public static String[] playlistList;
    public static String[] playlistIDs;
    private static User currentUser;
    private Spotify s;
    private Button start, stop;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Init stuff
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pref = this.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
        currentUser = new User();
        s = new Spotify(currentUser);
        start = (Button) findViewById(R.id.servStart);
        stop = (Button) findViewById(R.id.servStop);

        start.setOnClickListener(this);
        stop.setOnClickListener(this);

        //Authentication stuff
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Init Spotify Auth
        final AuthenticationRequest request = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI)
                .setScopes(new String[]{"user-read-private", "playlist-read", "playlist-read-private"})
                .build();

        //Check if user is authed
        TextView tv = (TextView)findViewById(R.id.loggedInUserText);
        if (tv.getText().equals("None")){
            currentUser.setUserIsLoggedIn(false);
        }

        if (!currentUser.getUserIsLoggedIn()) {
            AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        try{
        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    String token = response.getAccessToken();
                    currentUser.setUserOath(token);
                    currentUser.setUserIsLoggedIn(true);
                    onLogin();
                    break;

                // Auth flow returned an error
                case ERROR:
                 //   Log.d("TOKEN", "BAD");
                    currentUser.setUserIsLoggedIn(false);
                  //  Log.d("TOKEN RESPONSE", response.getError());
                    // Handle error response
                    TextView userText1 = (TextView) findViewById(R.id.loggedInUserText);
                    userText1.setText("Error Logging In");
                    break;

                // Most likely auth flow was cancelled
                default:
                    // Handle other cases
            }
        }
    } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onLogin(){
        //Set Textview with username
        s.setUserId();
        s.setDisplayName();
        TextView userText = (TextView)findViewById(R.id.loggedInUserText);
        userText.setText(currentUser.getUserDisplayName());

        Toast.makeText(MainActivity.this, "Logged in", Toast.LENGTH_LONG).show();

        //Save playlist to myprefs so its preselected next login
        try {
            Context context = MainActivity.this;
            SharedPreferences sharedPref = context.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
            currentUser.setUserCurrentPlaylistID(sharedPref.getString("prefPlaylistId", ""));
            currentUser.setUserCurrentPlaylistName(sharedPref.getString("prefPlaylistName", ""));
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText(currentUser.getUserCurrentPlaylistName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void savePreferences(){
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("prefPlaylistId", currentUser.getUserCurrentPlaylistID());
        edit.putString("prefPlaylistName", currentUser.getUserCurrentPlaylistName());
        edit.commit();
    }

    public void showSelectPlaylist(View view) {
        //Get playlist JSON and save it to two arrays
        final JSONArray items = s.getPlaylistList();
        playlistList = new String[items.length() + 1];
        playlistIDs = new String[items.length() + 1];

        try {
            for (int x = 0; x < items.length(); x++) {
                JSONObject oneitem = (JSONObject) items.get(x);
                playlistList[x + 1] = oneitem.getString("name");
                playlistIDs[x + 1] = oneitem.getString("id");
            }
            playlistList[0] = "Create New Playlist";
        }catch (JSONException ex){
            ex.printStackTrace();
        }

        //Popup asking user to select playlist
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog.Builder newPlaylist = new AlertDialog.Builder(this);
        final TextView t = (TextView) findViewById(R.id.textView);
        final EditText newPlaylistText = new EditText(this);

        builder.setTitle("Select Playlist to use")
                .setItems(playlistList, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        t.setText(playlistList[which]);
                        String playlistName = "";
                        if (which == 0){

                            newPlaylistText.setInputType(InputType.TYPE_CLASS_TEXT);
                            newPlaylist.setTitle("Create Playlist")
                                    .setView(newPlaylistText);
                            newPlaylist.setPositiveButton("Create", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    t.setText(newPlaylistText.getText().toString());
                                    s.createPlaylist(newPlaylistText.getText().toString());
                                }
                            });
                            newPlaylist.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            newPlaylist.show();
                        } else {
                            currentUser.setUserCurrentPlaylistID(playlistIDs[which]);
                            currentUser.setUserCurrentPlaylistName(playlistList[which]);
                        }
                    }
                }).create().show();
        savePreferences();
    }

    @Override
    public void onClick(View v) {
        if (v == start){
            Intent startIntent = new Intent(MainActivity.this, backgroundService.class);
            startService(startIntent);
        } else if (v == stop){
            stopService(new Intent(this , backgroundService.class));
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //stopService(new Intent(this , backgroundService.class));
        Log.d("SCLOSED", "Hit the onDestry method");
    }
}
