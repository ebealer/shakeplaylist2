This app will listen for any notifications dealing with music being played. It will allow the user to select a spotify playlist. Once a playlist is selected the app will stay running in the background. When a song comes on the users wishes to save while the phone is off/locked/on the user can shake the phone. The phone will emit a small vibration and the current playing song from various sources will be saved to the desired playlist. This is useful for saving songs while driving with no need to look at the phone.

TODO:
Enable the option to shake different sources to a spotify playlist. Ex. Youtube, pandora, native android player
